const express = require('express') 
const app = express()
const patRoutes = require('./routes/patients')
const perRoutes = require('./routes/personal')
const AtRoutes = require('./routes/atenciones')

require('./connection/mongoConnection')//conexión a base de datos

app.use(express.json())
app.use(express.urlencoded({ extended: false}))

app.use('/patients', patRoutes ) //}
app.use('/personal', perRoutes ) //}}Conexión a Routes
app.use('/atenciones', AtRoutes )//}

app.listen(5001, () => console.log('server running'))
