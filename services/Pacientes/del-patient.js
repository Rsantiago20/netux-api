const patModel = require('../../models/paciente')

const delPatient = async (patientId) => {
    try{
        const pat = await patModel.findByIdAndRemove(patientId)
        pat.save()
    return { status: 1, pat }
    }catch(err){
        return { status: 2, err }
    }
    
}

module.exports = delPatient