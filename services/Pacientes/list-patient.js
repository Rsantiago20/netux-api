const PatientModel = require('../../models/paciente')

const listPatient = async () => {
    const Patients = await PatientModel.find()
    return { Patients }
}

module.exports = listPatient