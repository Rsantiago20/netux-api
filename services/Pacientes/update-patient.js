const patModel = require ('../../models/paciente')

const updatePatient = async (patientId_data, nombres_data, apellidos_data, sexo_data,
   edad_data, fecha_nac_data, direccion_data, telefono_data) => {
    try{
        const update = { $set: { nombrea: nombres_data , apellidos: apellidos_data, sexo: sexo_data, edad: edad_data,
        fecha_nac:fecha_nac_data, direccion: direccion_data, telefono: telefono_data } }
        console.log(update)
        const patient = await patModel.findByIdAndUpdate( patientId_data, update)

        return { status: 1, patient }
  } catch (err) {
    return { status: 2, msg: 'data not updated'+ err }
  }
}

module.exports = updatePatient