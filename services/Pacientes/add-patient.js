const PatModel = require('../../models/paciente')

const addPatient = async (patData) => {
    try{
        response = await PatModel(patData).save()
    return { status: 1, msg: 'Patient aggregated' }
    }catch(err){
        return { status: 2, err }
    }
    
}

module.exports = addPatient