const PerModel = require('../../models/personal')

const addPersonal = async (perData) => {
    try{
        response = await PerModel(perData).save()
    return { status: 1, response }
    }catch(err){
        return { status: 2, err }
    }
    
}

module.exports = addPersonal