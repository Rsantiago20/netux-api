const perModel = require('../../models/personal')

const updatePersonal = async (perosnalId_data, nombre_data, especialidad_data, cargo_data) => {
    try{
        const update = { $set: { nombre: nombre_data, especialidad: especialidad_data, cargo: cargo_data } }
        console.log(update)
        const personal = await perModel.findByIdAndUpdate( perosnalId_data, update)
        return { status: 1, personal }
  } catch (err) {
    return { status: 2, msg: 'data not updated'+ err }
  }
}

module.exports = updatePersonal