const PerModel = require('../../models/personal')

const listPersonal = async () => {
    const personal = await PerModel.find()
    return { personal }
}

module.exports = listPersonal