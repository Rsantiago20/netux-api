const perModel = require('../../models/personal')

const delPersonal = async (personalId) => {
    try{
        const per = await perModel.findByIdAndRemove(personalId)
        per.save()
    return { status: 1, msg: 'personal deleted' }
    }catch(err){
        return { status: 2, msg: 'Personal cant be deleted' }
    }
    
}

module.exports = delPersonal