const perModel = require ('../../models/personal')
const axios = require('axios')

const getIps = async (personalId, nitIPS_data ) => {
    try{
        const Personal = await perModel.find({ _id: personalId })//busqueda de personal médico por Id
        
        const response = await axios ({  
            //consulta ips mediante nit
            method: 'get',
            url: 'https://www.datos.gov.co/resource/b4dp-ximh.json',
            params: {nit: nitIPS_data }

         })
         console.log(response.data)
         if(response.data.length > 0 ){
             console.log('nit valido')
         }else{
             return { status: 2, msg: 'nit no válido' }
         }
         return { status: 1, Personal }
    }catch(err){
        return { status: 2, msg: 'data not found'+ err }
    }
}


module.exports = getIps