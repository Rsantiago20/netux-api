const AtModel = require('../../models/citas')

const listAtPatient = async (patientId) => {
    try{
        const Patients = await AtModel.find({pacient_id: patientId})//consulta de citas mediante los id's en base de datos y el usado en el controller
        return { status: 1, Patients }
    }catch(err){
        return { status: 2, err }
    }
}

module.exports = listAtPatient