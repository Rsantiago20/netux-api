const AtModel = require('../../models/citas')

const updatePersonal = async (atentionId_data, fechahora_data) => {
    try{
        const update = { $set: { fecha_hora: fechahora_data } }
        const atention = await AtModel.findByIdAndUpdate( atentionId_data, update)
        return { status: 1, msg: 'date changed', atention }
  } catch (err) {
    return { status: 2, msg: 'data not updated' }
  }
}

module.exports = updatePersonal