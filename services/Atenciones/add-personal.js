const AtModel = require('../../models/citas') 

const addAtPersonal = async (atentionId, personalId) => {
    try{
        const atPersonal = await AtModel.findById(atentionId)//busqueda de la cita por id
        atPersonal.personal_id.push(personalId)
        atPersonal.save()//adición del personal tras buscar el id de la cita
        return{ return :1 , atPersonal }
    }catch(err){
        return { status: 2, msg: 'Personal not assigned' }
    }

}

module.exports = addAtPersonal