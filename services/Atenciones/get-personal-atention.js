const AtModel = require('../../models/citas')

const listAtPersonal = async (personalId) => {
    try{
        const Personal = await AtModel.find({personal_id: personalId})
        return { status: 1, Personal }
    }catch(err){
        return { status: 2, err }
    }
}

module.exports = listAtPersonal