const mongoose = require('mongoose')
const Schema = mongoose.Schema

const personalSchema = new Schema ({
	nombre:	String,
	ips:	String,
	nitIPS: Number,
	especialidad: String,
	cargo:	String,
})

const personalModel = mongoose.model('personal', personalSchema)
module.exports = personalModel