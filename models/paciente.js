const mongoose = require('mongoose')
const Schema = mongoose.Schema

const pacienteSchema = new Schema({
    nombres: String,
	apellidos: String,
	doc_identidad: Number,
	sexo: String,
	edad: Number,
	fecha_nac: Date,
	direccion: String,
	telefono: Number,
})

const patModel = mongoose.model('pacientes', pacienteSchema)

module.exports = patModel