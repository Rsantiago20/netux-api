const mongoose = require('mongoose')
const Schema = mongoose.Schema

const citasSchema = new Schema ({
	fecha_hora:	Date,
	form: {
		presion_sanguínea: Number,
		pulso_cardíaco: Number,
		peso: Number,
        observaciones_médico: String,
	},
	pacient_id: [String],
	personal_id: [String]
})

const citasModel = mongoose.model('citas', citasSchema)
module.exports = citasModel