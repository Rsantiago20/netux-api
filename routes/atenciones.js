var router = require('express').Router()
const { newAtention, newAtPatient, newAtPersonal, listAt, listAtPatient, updateAt, listAtPersonal } = require('../controllers/atentionsController')

router.post('/add', newAtention)
router.post('/addpacient', newAtPatient)
router.post('/addpersonal', newAtPersonal)
router.get('/', listAt)
router.get('/getpatients', listAtPatient)
router.get('/getpersonal', listAtPersonal)
router.put('/update', updateAt)

module.exports = router