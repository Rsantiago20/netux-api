var router = require('express').Router()
const { newPer, updatePer, listPer, deletePer, getIps } = require('../controllers/personalController')

router.post('/addPer', newPer)
router.put('/update', updatePer)
router.get('/', listPer)
router.get('/getips', getIps)
router.delete('/delete', deletePer)

module.exports = router
