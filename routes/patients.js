var router = require('express').Router()
const { newPat, updatePat, listPat, deletePat } = require('../controllers/patientsController')

router.post('/addPac', newPat)
router.put('/update', updatePat)
router.get('/', listPat)
router.delete('/delete', deletePat)

module.exports = router
