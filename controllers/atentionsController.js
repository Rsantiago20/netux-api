const newAtentionPatientService = require('../services/Atenciones/add-patient')
const newAtentionPersonalService = require('../services/Atenciones/add-personal')
const newAtentionService = require('../services/Atenciones/add-atention')
const listAtService = require('../services/Atenciones/list-atencion')
const listAtPatientService = require('../services/Atenciones/get-patient-atention')
const listAtPersonalService = require('../services/Atenciones/get-personal-atention')
const updateAtService = require('../services/Atenciones/update-atencion')

const newAtention = async (req, res) =>{
    const Atention = req.body
    const response = await newAtentionService(Atention)
    res.json(response)
}
const newAtPatient = async (req, res) =>{
    const { atentionId, patientId } = req.body 
    const response = await newAtentionPatientService(atentionId, patientId)
    res.json(response)
}

const newAtPersonal = async (req, res) =>{
    const { atentionId, personalId } = req.body
    const response = await newAtentionPersonalService(atentionId, personalId)
    res.json(response)
}

const listAt = async (req, res) =>{
    const response = await listAtService()
    res.json(response)
}

const listAtPatient = async (req, res) =>{
    const { patientId } = req.body
    const response = await listAtPatientService(patientId)
    res.json(response)
}

const listAtPersonal = async (req, res) =>{
    const { personalId } = req.body
    const response = await listAtPersonalService(personalId)
    res.json(response)
}

const updateAt = async (req, res) =>{
    const { atentionId } = req.body
    const { fechahora } = req.body
    const response = await updateAtService (atentionId, fechahora)
    res.json(response)
}

module.exports = {
    newAtPatient,
    newAtention,
    newAtPersonal,
    listAt,
    listAtPatient,
    updateAt,
    listAtPersonal
    
}