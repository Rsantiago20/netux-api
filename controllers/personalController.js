const newPerService = require('../services/Personal/add-personal')
const listPerService = require('../services/Personal/list-personal')
const updatePerService = require('../services/Personal/update-personal')
const deletePerService = require('../services/Personal/del-personal')
const getIpsService = require('../services/Ips/get-ips')

//Agregar nuevo personal médico
const newPer = async (req, res) =>{
    const per = req.body
    const response = await newPerService(per)
    res.json(response)
}

//Buscar personal médico
const listPer = async (req, res) =>{
    const response = await listPerService()
    res.json(response)
}

//Actualizar datos del personal médico
const updatePer = async (req, res) =>{
    const { personalId } = req.body
    const { nombre } = req.body
    const { especialidad } = req.body
    const { cargo } = req.body
    const response = await updatePerService (personalId, nombre, especialidad, cargo)
    res.json(response)
}

//consulta de la Ips del personal médico mediante nit y id de la base de datos
const getIps = async (req, res) =>{
    const { personalId } = req.body
    const { nitIPS } = req.body
    console.log
    const response = await getIpsService (personalId, nitIPS)
    res.json(response)
}

//Eliminar Personal Medico
const deletePer = async (req, res) =>{
    const { personalId } = req.body
    const response = await deletePerService( personalId )
    res.json(response)
}

module.exports = {
    newPer,
    listPer,
    updatePer,
    deletePer,
    getIps
}