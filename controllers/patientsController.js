const newPatService = require('../services/Pacientes/add-patient')
const listPatService = require('../services/Pacientes/list-patient')
const updatePatService = require('../services/Pacientes/update-patient')
const deletePatService = require('../services/Pacientes/del-patient')

//Nuevo paciente
const newPat = async (req, res) =>{
    const pat = req.body
    const response = await newPatService(pat)
    res.json(response)
}

//buscar paciente
const listPat = async (req, res) =>{ 
    const response = await listPatService()
    res.json(response)
}

//actualizar datos del paciente
const updatePat = async (req, res) =>{
    const { nombres } = req.body
    const { apellidos } = req.body
    const { sexo } = req.body
    const { edad } = req.body
    const { fecha_nac } = req.body
    const { direccion } = req.body
    const { telefono } = req.body
    const { patientId } = req.body
    console.log({ patientId, apellidos })
    const response = await updatePatService (patientId, nombres, apellidos, sexo, edad, fecha_nac, direccion, telefono )
    res.json(response)
}

//eliminar paciente
const deletePat = async (req, res) =>{
    const { patientId } = req.body
    const response = await deletePatService( patientId )
    res.json(response)
}

module.exports = {
    newPat,
    listPat,
    updatePat,
    deletePat
}