# Prueba Técnica
Realización prueba técnica para Netux.

# Services
Atenciones = Citas
Ips
Pacientes
Personal

# Routes :

## Atenciones
POST: /add
(Creación de cita )
POST: /addpacient
(adición de un paciente a la cita )
POST: /addpersonal 
(adición de un médico a la cita )
GET: /
(consulta de citas)
GET: /getpatients
(consulta de citas por paciente)
GET: /getpersonal
(consulta de citas por médico)
PUT: /update
(actualización de cita, solo se puede modificar la fecha y hora de la cita)

## Personal
POST: /addPer
PUT: /update
GET: /
GET: /getips
DELETE: /delete

## Pacientes
POST: /addPac
PUT: /update
GET: /
DELETE: /delete

# Controllers 
AtentionsController
PatientsController
PersonalController